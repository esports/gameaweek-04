﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour {
	public float speed;

	void Update () {
		CharacterController controller = GetComponent<CharacterController>();
		
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit = new RaycastHit();
		Vector3 lookTarget = new Vector3(0f, 0f, 0f);
	    if (Physics.Raycast (ray, out hit)) {
	        lookTarget = hit.point;
	    }
		
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		controller.SimpleMove(speed * (new Vector3(h, 0, v)));
		transform.LookAt(lookTarget);
	
	}
}
